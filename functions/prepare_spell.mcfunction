#shot delay
execute as @a[scores={shotCharge=-10..}] at @s run function rogues-basis:characters/emma/active/shotdelay

execute as @e[tag=lshot] at @s run function rogues-basis:projectile/bolt/lshotf
execute as @e[tag=pshot] at @s run function rogues-basis:projectile/thorns/pshotmove

scoreboard players add @e[tag=lshot] shotT 1
scoreboard players add @e[tag=pshot] shotT 1

kill @e[scores={shotT=350..}]

#thorns
execute as @e[tag=thorns] at @s run function rogues-basis:projectile/spell_thorns

#frost pillar
execute as @e[type=arrow] at @s run function rogues-basis:projectile/spell_arrow

#frost pillar
execute as @e[tag=frostwall] at @s run function rogues-basis:projectile/spell_frostpil

#wave
execute as @e[tag=wave] at @s run function rogues-basis:projectile/spell_wave

#blackhole
execute as @e[tag=blackhole] at @s run function rogues-basis:projectile/spell_blackhole

#fire breath projectile
execute as @e[type=armor_stand,name=Fire] at @s run function rogues-basis:projectile/spell_fireb

#web shot
execute as @e[name=web] at @s run function rogues-basis:projectile/spell_web

#decoy
execute as @e[tag=smoker] at @s run function rogues-basis:projectile/decoy/spell_smoke
execute as @e[tag=smokerloc] at @s run function rogues-basis:projectile/decoy/spell_smokeloc
kill @e[tag=smoker]

#portal
execute as @e[tag=portalset] at @s run particle minecraft:witch ~ ~.5 ~ .2 .3 .2 0 1 force

#smoke bomb
execute as @e[tag=sb] at @s run function rogues-basis:projectile/spell_smokebomb

#hurricane
execute as @e[tag=hurpro] at @s run function rogues-basis:projectile/spell_hurpro

#soul send
execute as @e[tag=soulsend] at @s run function rogues-basis:projectile/spell_soulsend
execute as @e[tag=soulsend] at @s run function rogues-basis:projectile/spell_soulsend

#totem
execute as @e[tag=totem] at @s run function rogues-basis:characters/vohelm/active/spell_totemactive

#gernade
execute as @e[tag=gernade] at @s run function rogues-basis:characters/barbose/active/spell_geractive

#jump pad
execute as @e[tag=jp] at @s run function rogues-basis:characters/pluto/spells/spell_jpactive

#spider
execute as @e[type=cave_spider] at @s run function rogues-basis:characters/halt/active/spell_spider

#trident
execute as @e[type=trident] at @s run function rogues-basis:projectile/spell_trident

#spike
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot
execute as @e[tag=spike] at @s run function rogues-basis:characters/pluto/spells/spell_spikeshot

#totem
scoreboard players add @e[tag=totem] totemtimer 1
kill @e[tag=totem,scores={totemtimer=800..}]