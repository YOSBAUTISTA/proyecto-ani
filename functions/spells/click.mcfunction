execute as @s[scores={class=1}] at @s run function rogues-basis:characters/sparket/spells
execute as @s[scores={class=2}] at @s run function rogues-basis:characters/vohelm/spells
execute as @s[scores={class=3}] at @s run function rogues-basis:characters/emma/spells
execute as @s[scores={class=4}] at @s run function rogues-basis:characters/pluto/spells
execute as @s[scores={class=5}] at @s run function rogues-basis:characters/halt/spells
execute as @s[scores={class=6}] at @s run function rogues-basis:characters/barbose/spells

execute as @s[gamemode=creative] at @s run function rogues-basis:characters/sparket/spells
execute as @s[gamemode=creative] at @s run function rogues-basis:characters/vohelm/spells
execute as @s[gamemode=creative] at @s run function rogues-basis:characters/emma/spells
execute as @s[gamemode=creative] at @s run function rogues-basis:characters/pluto/spells
execute as @s[gamemode=creative] at @s run function rogues-basis:characters/halt/spells
execute as @s[gamemode=creative] at @s run function rogues-basis:characters/barbose/spells

execute as @s[tag=lobby] at @s run function rogues-basis:spells/select_spirit

execute as @s[nbt={SelectedItem:{id:"minecraft:book"}}] run scoreboard players set @s venge 300
execute as @s[nbt={SelectedItem:{id:"minecraft:book"}}] run tag @s add vengeuse

function rogues-basis:spells/update_hotbar

scoreboard players set @s presstut -500

scoreboard players set @s pressdelay 0
scoreboard players set @s press 0